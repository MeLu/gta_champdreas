// Specify that we use version 3.3 of OpenGL
#version 330 core
in vec2 TexCoords;
in vec3 Normal;
in vec3 FragPos;

out vec4 color;

uniform sampler2D modelTexture;
uniform vec3 lightPos;
uniform vec3 lightColor;
uniform vec3 viewPos;
uniform float specularStrength;
uniform float ambiantStrenght;

void main()
{
	// Ambient
    vec3 ambient = ambiantStrenght * lightColor;

    // //Diffuse
    vec3 norm = normalize(Normal);
    vec3 lightDir = normalize(lightPos - FragPos);
    float diff = max(dot(norm, lightDir), 0.0);
    vec3 diffuse = diff * lightColor;

    vec4 result = vec4(ambient + diffuse, 1.0f);
    color = result * texture(modelTexture, TexCoords);
}

    // // Specular
    // vec3 viewDir = normalize(viewPos - FragPos);
    // vec3 reflectDir = reflect(-lightDir, norm );
    // float spec = pow(max(dot(viewDir, reflectDir), 0.0) , 32);
    // vec3 specular = specularStrength * spec * lightColor + specularStrength * specLamp * lampColor ;
    // vec3 result = (ambient + specular + diffuse);