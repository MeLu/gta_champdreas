# READ ME

## Our Project 

We are two students from ENSG (Ecole Nationale des Sciences Géographiques), this is the repository for the openGL project 
which we had to do to validate the openGL course.  

The two students are Melvin Pichenot (melvin.pichenot@ensg.eu) and Florentin Brisebard (florentin.brisebard@ensg.eu)
 

## Installation

In order to install the project, first you have to clone or download it into a folder.

Then you have to dezip the bin_external_lib.zip folder and put those three folder (bin, external and lib) in the root folder.

Once this is done, open a terminal in the folder and then type this : 

`mkdir build && cd build `

then

`cmake ..`

then

`make`

then

`./exercice`
