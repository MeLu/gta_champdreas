#include <iostream>
#include <string>
#include <vector>
#include <ctime>
#include <map>

#include <stdio.h>  /* printf, scanf, puts, NULL */
#include <stdlib.h> /* srand, rand */
#include <time.h>
#include <cmath>

// GLEW
#include <GL/glew.h>
// GLFW
#include <GLFW/glfw3.h>

// GLM Mathematics
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

// Shader loading class
#include "Shader.h"
// Camera loading class
#include "Camera.h"
// Model loading class
#include "Model.h"

// Other Libs
#include <SOIL.h>

using namespace std;

// Dimension of the window
const GLuint WIDTH = 800, HEIGHT = 800;


// Prototype of functions
void fillBuffer(GLuint &VAO, GLuint &VBO, GLuint &EBO, GLfloat vertices[],
                GLshort indices[], GLint vsize, GLint isize);
void drawObject(GLuint &VAO, GLint nVertex, GLuint &texture, Shader &shaders);
void loadTexture(GLuint &texture, const char *path);

// Main function, it is here that we initialize what is relative to the context
// of OpenGL and that we launch the main loop
int main()
{
    std::cout << "Starting GLFW context, OpenGL 3.3" << std::endl;
    // Initialization of GLFW
    glfwInit();

    // Specify the OpenGL version to be used (3.3)
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3); //3
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3); //.3
    // Disable the deprecated functions
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
// Necesary for Mac OS
#ifdef __APPLE__
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
#endif
    // Prevent the change of dimension of the window
    glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);

    // Create the application window
    GLFWwindow *window = glfwCreateWindow(WIDTH, HEIGHT, "MyWindow", nullptr, nullptr);
    if (window == nullptr)
    {
        std::cout << "Failed to create GLFW window" << std::endl;
        glfwTerminate();
        return -1;
    }

    // Associate the created window with the OpenGL context
    glfwMakeContextCurrent(window);
    // Associate the callback with the pressure of a key on the keyboard
    glfwSetKeyCallback(window, key_callback);

    // Variable global that allows to ask the GLEW library to find the modern
    // functions of OpenGL
    glewExperimental = GL_TRUE;
    // Initializing GLEW to retrieve pointers from OpenGL functions
    if (glewInit() != GLEW_OK)
    {
        std::cout << "Failed to initialize GLEW" << std::endl;
        return -1;
    }

    // Transform the window dimensions to the dimensions of OpenGL (between -1 and 1)
    int width, height;
    // Recover the dimensions of the window created above
    glfwGetFramebufferSize(window, &width, &height);
    glViewport(0, 0, width, height);

    // Allow to test the depth information
    glEnable(GL_DEPTH_TEST);

    // Build and compile our vertex and fragment shaders
    Shader shaders("shaders/vertexshader.vs", "shaders/fragmentshader.fs");
    Shader sunshader("shaders/sun.vs", "shaders/sun.fs");
    Shader moonshader("shaders/moon.vs", "shaders/moon.fs");

    // Load the model
    Model moon((char *)"model/moon/planet.obj");
    Model man((char *)"model/nanosuit/nanosuit.obj");
    Model sun((char *)"model/sun/planet.obj");

    // Ground geometry separated (position + uv)
    GLfloat groundVertices[] = {
        // coords       // UV
        -20, -4, 20,    0, 0,
        20, -4,  20,    1, 0,
        20, -4, -20,    1, 1,
        -20,-4, -20,    0, 1,
    };

    // ground
    GLshort groundIndices[] = {
        0, 2, 3,
        0, 1, 2,
    };

    // Declare the identifiers of our VAO, VBO and EBO
    GLuint VAO, VBO, EBO;
    // Inform OpenGL to generate one VAO
    glGenVertexArrays(1, &VAO);
    // Inform OpenGL to generate one VBO
    glGenBuffers(1, &VBO);
    // Inform OpenGL to generate one EBO
    glGenBuffers(1, &EBO);

    // Fill the buffers with each object
    fillBuffer(VAO, VBO, EBO, groundVertices, groundIndices, sizeof(groundVertices), sizeof(groundIndices));

    // Declare the texture identifier
    GLuint texture;
    // Generate the texture
    glGenTextures(1, &texture);
    // Load the texture
    loadTexture(texture, "texture/grass.jpg"); // ground

    // Camera
    Camera camera(glm::vec3(0.0f, 4.0f, 10.0f), window);
    GLfloat ambiantStrenght(0.05f);

    //Changes of the light of the day because of the rotation of the sun
    GLfloat ANGLE_DUSK(3 * M_PI / 5);
    GLfloat ANGLE_DOWN(4 * M_PI / 3);

    //Duration of the cycle night/day. 120 = 2 mins of day + 2 mins of night
    GLint CYCLE(120);

    // Light Color
    glm::vec3 lightColor(1.0f, 1.0f, 1.0f);

    // The main loop of the program
    while (!glfwWindowShouldClose(window))
    {
        // Retrieve events and call the corresponding callback functions
        glfwPollEvents();
        camera.Do_Movement();
        camera.Switch_view();

        // Replace the background color buffer of the window
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);


        // Camera matrix
        glm::mat4 view;
        view = camera.GetViewMatrix();
        // Projection matrix (generate a perspective projection matrix)
        glm::mat4 projection;
        projection = glm::perspective(45.0f, (GLfloat)WIDTH / (GLfloat)HEIGHT, 0.1f, 100.0f);

        /*
        Drawing the Sun
        */
       // using the sun shader
        sunshader.Use();
        // Defining the model of the rotation
        GLfloat timeValue = glfwGetTime();
        GLfloat sunRadius = 5.0f;
        glm::vec3 sunPos(sunRadius * cos(timeValue / 2 + (GLfloat)M_PI/2), sunRadius * sin(timeValue / 2 + (GLfloat)M_PI/2), -15);
        glm::mat4 model = glm::mat4(1.0f);
        model = glm::mat4(1.0f);
        model = glm::translate(model, sunPos);
        model = glm::scale(model,glm::vec3(0.5f));
        //  Setting shader value
        sunshader.setMat4("projection", projection);
        sunshader.setMat4("view", view);
        sunshader.setMat4("model", model);
        // Draw sun
        sun.Draw(sunshader);

        // Setting the values of the moon shader because in space, there's no ambiant light
        moonshader.Use();
        moonshader.setVec3("lightPos", sunPos);
        moonshader.setVec3("lightColor", lightColor);
        moonshader.setMat4("view", view);
        moonshader.setMat4("projection", projection);

        shaders.Use();

        // Model matrix (translation, rotation and scale)
        model = glm::mat4(1.0f);
        model = glm::translate(model, glm::vec3(0.0f));
        model = glm::scale(model, glm::vec3(0.1f));

        // Update the global variables of the shader of the Static scene ("shaders")
        shaders.setMat4("projection", projection);
        shaders.setMat4("view", view);
        shaders.setMat4("model", model);
        shaders.setFloat("ambiantStrenght", ambiantStrenght);
        // Setting the values of default shaders 
        shaders.setVec3("lightPos", sunPos);
        shaders.setVec3("lightColor", lightColor);


        // Draw objects of the scene
        man.Draw(shaders);
        // ground
        drawObject(VAO, 2, texture, shaders);
        
        /* Draw the moon plus setting the twilight
        glm::mod(...) unitary matrix to the model matrix (restart the position of the model)
        */
        GLfloat radius = 15.0f;
        model = glm::mat4(1.0f);
        model = glm::translate(model, glm::vec3(radius * cos(timeValue / 2), radius * sin(timeValue / 2), -25));
        model = glm::scale(model, glm::vec3(0.4f));

        // Update the global variable of the shader
        moonshader.setMat4("model", model);


        // Redraw the object
        moon.Draw(moonshader);

        // Setting of the dayLight according to the angle of the sun
        double angle_double =  (double)timeValue / 2 - floor((double)timeValue/(4*M_PI))*2*M_PI;
        glm::vec3 clearColor;
        GLfloat angle = (float)angle_double;
        if (angle > ANGLE_DUSK && angle < ANGLE_DOWN)
        {
            // lmpColor = glm::vec3(1.0f, 0.56f, 0.17f);
            // lColor = glm::vec3(0.0f, 0.0f, 0.05f);
            clearColor = glm::vec3(0.0f, 0.0f, 0.1f);
            ambiantStrenght = 0.05f;

            //
        }
        else if (angle > M_PI / 4 && angle < ANGLE_DUSK)
        //sunrise
        {
            // lmpColor = glm::vec3(1.0f, 0.56f, 0.17f);
            double x = (ANGLE_DUSK - angle) / (ANGLE_DUSK - M_PI / 4);
            // lColor = glm%(2*pi)::vec3(x * 1.0f, pow(x, 2) * 1.0f, pow(x, 4) * 1.0f + 0.05f);
            clearColor = glm::vec3((-2 * pow(x, 2) + 2 * x) * 1.0f, pow(x, 2) * 0.5f, pow(x, 4) * 1.0f + 0.1f);
            ambiantStrenght = x * 0.65f + 0.05f;
        }
        else if (angle > ANGLE_DOWN && angle < 5 * M_PI / 3)
        //Day light
        {
            // lmpColor = glm::vec3(1.0f, 0.56f, 0.17f);
            double x = (ANGLE_DOWN - angle) / (ANGLE_DOWN - 5 * M_PI / 3);
            // lColor = glm::vec3(pow(x,4) * 1.0f, pow(x, 2) * 1.0f, x * 1.0f + 0.05f);
            clearColor = glm::vec3((-2 * pow(x, 2) + 2 * x) * 1.0f, pow(x, 2) * 0.5f, pow(x, 4) * 1.0f + 0.1f);
            ambiantStrenght = x * 0.65f + 0.05f;
        }
        else
        {
            // lmpColor = glm::vec3(0.0f, 0.0f, 0.0f);
            // lColor = glm::vec3(1.0f, 1.0f, 1.0f);
            clearColor = glm::vec3(0.0f, 0.5f, 1.0f);
            ambiantStrenght = 0.9f;
        }
        // Defining ambient light with glClearColor
        glClearColor(clearColor.x, clearColor.y, clearColor.z, 1.0f);
        shaders.setFloat("ambiantStrenght", ambiantStrenght);



        // Exchange rendering buffers to update the window with new content
        glfwSwapBuffers(window);
    }

    // Delete the objects and buffer that we created earlier
    glDeleteVertexArrays(1, &VAO);
    glDeleteBuffers(1, &VBO);
    glDeleteBuffers(1, &EBO);

    // End of the program, we clean up the context created by the GLFW
    glfwTerminate();
    return 0;
}

void fillBuffer(GLuint &VAO, GLuint &VBO, GLuint &EBO, GLfloat vertices[],
                GLshort indices[], GLint vsize, GLint isize)
{
    // Bind the VAO
    glBindVertexArray(VAO);

    // Bind and fill the VBO
    glBindBuffer(GL_ARRAY_BUFFER, VBO);
    glBufferData(GL_ARRAY_BUFFER, vsize, vertices, GL_STATIC_DRAW);
    // Bind and fill the EBO
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, isize, indices, GL_STATIC_DRAW);

    // Position attribute
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 5 * sizeof(GLfloat), (GLvoid *)0);
    glEnableVertexAttribArray(0);
    // UV attribute
    glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 5 * sizeof(GLfloat),
                          (GLvoid *)(3 * sizeof(GLfloat)));
    glEnableVertexAttribArray(1);

    // VBO is detached from the current buffer in the OpenGL context
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    // VAO is detached from the current object in the OpenGL context
    glBindVertexArray(0);
    // EBO is detached (the last one!)
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
}

void drawObject(GLuint &VAO, GLint nVertex, GLuint &texture, Shader &shaders)
{
    // Bind the VAO of house as a current object in the context of OpenGL
    glBindVertexArray(VAO);
    // Activate texture
    glActiveTexture(GL_TEXTURE0);
    // Bind the texture
    glBindTexture(GL_TEXTURE_2D, texture);
    // Associate the texture with the shader
    glUniform1i(glGetUniformLocation(shaders.Program, "modelTexture"), 0);
    // Draw the current object
    glDrawElements(GL_TRIANGLES, 3 * nVertex, GL_UNSIGNED_SHORT, 0);
    // VBA is detached from the current object in the OpenGL context
    glBindVertexArray(0);
}

void loadTexture(GLuint &texture, const char *path)
{
    // Bind the texture created in the global context of OpenGL
    glBindTexture(GL_TEXTURE_2D, texture);
    // Method of wrapping
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    // Filtering method
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

    // Loading the image file using the SOIL lib
    int twidth, theight;
    unsigned char *data = SOIL_load_image(path, &twidth, &theight, 0, SOIL_LOAD_RGB);
    // Associate the image data with texture
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, twidth, theight,
                 0, GL_RGB, GL_UNSIGNED_BYTE, data);
    // Generate the mipmap
    glGenerateMipmap(GL_TEXTURE_2D);
    // Free the memory
    SOIL_free_image_data(data);
    // Unbind the texture
    glBindTexture(GL_TEXTURE_2D, 0);
}


// bool CheckCollision(Model &one, Model &two) // AABB - AABB collision
// {
//     // collision x-axis?
//     bool collisionX = one.Position.x + one.Size.x >= two.Position.x &&
//         two.Position.x + two.Size.x >= one.Position.x;
//     // collision y-axis?
//     bool collisionY = one.Position.y + one.Size.y >= two.Position.y &&
//         two.Position.y + two.Size.y >= one.Position.y;
//     // collision only if on both axes
//     return collisionX && collisionY;
// }